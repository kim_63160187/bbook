/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.bookstore;

/**
 *
 * @author ASUS
 */
public class Book {
    // กำหนดคุณสมบัติใหม่
    private String title ="Name book" ;
    private int cost = 100;
    private String type;

//กำหนดค่าตัวแปรให้เป็นconstructor
    Book(String title, int cost,String type) {
        this. title =  title;
        this.cost = cost;
        this.type = type;
    }
    
// กำหนดการทำงานของ discount คือ ลดราคาสินค้า 20%    
public double discount() {
         return cost - (cost * 0.20);
        }

//แสดงผลราคาหนังสือที่ลด
public void showDetails() {
    System.out.print("Name Book : " + title);
    System.out.println("Cost(sale 20%) : " + discount()+ " Bath");
 }

}
